import org.testng.Assert;
import org.testng.annotations.*;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EpointTest extends BaseTest {

    @FindBy(xpath = "//*[@class='search-icon']")
    private WebElement searchIcon;

    @FindBy(className = "solr-search-modal")
    private WebElement searchModal;

    @FindBy(id = "query")
    private WebElement queryInput;

    @FindBy(xpath = "//input[@class='gsc-input']")
    private WebElement queryInputEnter;

    @FindBy(className = "gsc-result")
    private List<WebElement> results;

    @FindBy(className = "gsc-results-wrapper-nooverlay")
    private WebElement resultsWrapper;

    @FindBy(className = "gsc-cursor-page")
    private List<WebElement> pages;

    @FindBy(className = "gsc-clear-button")
    private WebElement clearButton;

    @Test(dataProvider = "getData")
    public void epointSite(String searchText)  {
        PageFactory.initElements(driver,this);
        driver.get("https://www.e-point.pl/");
        Assert.assertEquals(driver.getTitle(), "Strona główna | e-point SA");

        searchIcon.click();
        Assert.assertTrue(searchModal.isDisplayed());

        queryInput.sendKeys(searchText);
        queryInput.sendKeys(Keys.ENTER);
        String URL = driver.getCurrentUrl();
        Assert.assertEquals(URL, "https://www.e-point.pl/wyniki-wyszukiwania?q=test");
        Assert.assertEquals(queryInputEnter.getAttribute("value"), searchText);
        Assert.assertFalse(results.isEmpty());

        List<WebElement> prevResultText = new ArrayList<>(results);
        WebElement page2 = pages.get(1);
        page2.click();
        PageFactory.initElements(driver,this);
        Assert.assertEquals(queryInputEnter.getAttribute("value"), searchText);
        Assert.assertFalse(results.isEmpty());
        Assert.assertNotEquals(prevResultText, results);

        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", clearButton);
        Assert.assertTrue(queryInputEnter.getAttribute("value").isEmpty());
        Assert.assertFalse(resultsWrapper.isDisplayed());
    }

    @DataProvider
    public Object[][] getData() {
        return new Object[][] {{"test"}};
    }
}
