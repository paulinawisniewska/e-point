import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

public class BaseTest {

    protected WebDriver driver;

    @BeforeTest
    public void before() {
        String driverPath = "C:\\Users\\rw-da\\Desktop\\Selenium\\src\\main\\executable\\drivers\\chromedriver.exe";
        System.setProperty("webdriver.chrome.driver", driverPath);
        driver = new ChromeDriver();
    }

    @AfterTest
    public void after() {
        driver.quit();
    }
}
